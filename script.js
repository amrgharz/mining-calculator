function primeState(){

    request = new XMLHttpRequest()

    request.open("GET", "https://cors-anywhere.herokuapp.com/https://www.coincalculators.io/api?name=siaprime");
    request.onload = function() {

        // Begin accessing JSON data here

        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {
            document.getElementById('name').innerHTML = (data.name)
            document.getElementById('hash-rate').innerHTML =  (data.currentNethash)
            document.getElementById('price-btc').innerHTML = (data.price_btc * 1000)
            document.getElementById('price-usd').innerHTML = (data.price_usd * 1000)
        }else{
            console.log('error')
        } 
        let profitability = function(){

            //Get the user input data

            let hashRate = document.getElementById("hashRate").value;

            //Prosess user's input data

            let hashrateGhash = hashRate * 1000000000000;
            let difficulty = data.currentDifficulty;
            let primeBlockTime = data.blockTime;
            let blockHeight = data.lastBlock;
            let period = 3600;
            
            let hourlyResult = (hashrateGhash / (difficulty / primeBlockTime)) * ((300000 - blockHeight - ((period / primeBlockTime) / 2)) * (period / primeBlockTime) * 0.8);
            let dailyResult = hourlyResult * 24;
            let weeklyResult = dailyResult * 7;
            let monthlyResult = weeklyResult * 4;
            let yearlyResult = monthlyResult * 12;

            document.getElementById("hourlyResult").innerHTML = hourlyResult;
            document.getElementById("dailyResult").innerHTML = dailyResult;
            document.getElementById("weeklyResult").innerHTML = weeklyResult;
            document.getElementById("monthlyResult").innerHTML = monthlyResult;
            document.getElementById("yearlyResult").innerHTML = yearlyResult;

            // Revenue in BTC

            let hourlyRevBtc = hourlyResult * (data.price_btc);
            let dailyRevBtc = hourlyRevBtc * 24;
            let weeklyRevBtc = dailyRevBtc * 7;
            let monthlyRevBtc = weeklyRevBtc * 4;
            let yearlyRevBtc = monthlyRevBtc * 12;
            document.getElementById('hourlyRevBtc').innerHTML = hourlyRevBtc;
            document.getElementById('dailyRevBtc').innerHTML = dailyRevBtc;
            document.getElementById('weeklyRevBtc').innerHTML = weeklyRevBtc;
            document.getElementById('monthlyRevBtc').innerHTML = monthlyRevBtc;
            document.getElementById('yearlyRevBtc').innerHTML = yearlyRevBtc;


            // Revenue In Usd

            let hourlyRevUsd = hourlyResult * (data.price_usd);
            let dailyRevUsd = hourlyRevUsd * 24;
            let weeklyRevUsd = dailyRevUsd * 7;
            let monthlyRevUsd = weeklyRevUsd * 4;
            let yearlyRevUsd = monthlyRevUsd * 12;
            document.getElementById('hourlyRevUsd').innerHTML = hourlyRevUsd;
            document.getElementById('dailyRevUsd').innerHTML = dailyRevUsd;
            document.getElementById('weeklyRevUsd').innerHTML = weeklyRevUsd;
            document.getElementById('monthlyRevUsd').innerHTML = monthlyRevUsd;
            document.getElementById('yearlyRevUsd').innerHTML = yearlyRevUsd;
        
        }
        document.getElementById("calculate").addEventListener('click', profitability )
    }
    request.send()
} 
primeState()


